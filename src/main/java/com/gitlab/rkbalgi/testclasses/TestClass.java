package com.gitlab.rkbalgi.testclasses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestClass {

  private static final Logger LOG = LoggerFactory.getLogger(TestClass.class);
  public int acc = 0;

  public void testLogWithGuard() {

    if (LOG.isDebugEnabled()) {
      LOG.debug("with a guard {}", acc);
    }

    acc++;
  }

  public void testLogWithoutGuard() {

    LOG.debug("with a guard {}", acc);
    acc++;

  }

  public void testLogNoLog() {
    acc++;
  }


}
